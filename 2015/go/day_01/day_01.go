package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func solve(data []string, part int) int64 {
	var count int64
	var offset int64
	d := strings.Split(data[0], "")
	for i := 0; i < len(d); i++ {
		if part == 2 && count == -1 {
			offset = int64(i)
			break
		}
		if d[i] == "(" {
			count = count + 1
		} else {
			count = count - 1
		}
	}

	if part == 1 {
		return count
	} else {
		return offset
	}
}

///
/// Main
///
func main() {
	fmt.Println("AOC 2015 Day 1")
	file, err := os.Open("puzzle.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}

	var data []string
	for scanner.Scan() {
		data = append(data, scanner.Text())
	}

	fmt.Println("Part one: ", solve(data, 1))
	fmt.Println("Part two: ", solve(data, 2))
}
